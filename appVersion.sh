vapp=$1
vhelm=$2

sed -i "s/^appVersion:.*$/appVersion: ${vapp}/" Helm/Chart.yaml
sed -i "s/^version:.*$/version: ${vhelm}/" Helm/Chart.yaml
