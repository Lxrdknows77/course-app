#!/bin/bash

version="$1"

major=0
minor=0
patch=0

# break down the version number into it's components
regex="([0-9]+).([0-9]+).([0-9]+)"
if [[ $version =~ $regex ]]; then
  major="${BASH_REMATCH[1]}"
  minor="${BASH_REMATCH[2]}"
  patch="${BASH_REMATCH[3]}"
fi

# check paramater to see which number to increment
if [[ "$2" == "nextMinor" ]]; then
  minor=$(echo $minor + 1 | bc) 
  patch=0
elif [[ "$2" == "nextPatch" ]]; then
  patch=$(echo $patch + 1 | bc)
elif [[ "$2" == "nextMajor" ]]; then
  major=$(echo $major+1 | bc)
  minor=0 && patch=0
else
  patch=$patch
  minor=$minor
  major=$major
fi

# echo the new version number
echo "${major}.${minor}.${patch}"

